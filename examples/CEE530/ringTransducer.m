function [Th,impulseResponse] = ringTransducer(FIELD_PARAMS, oR, iR, dTheta, numR)
% setting the fixed excitation for the ring transducer
fractionalBandwidth = 0.5;
centerFrequency = 10e6;

dr = oR-iR;
focus = 8e-3;
phi = atan((dr/2)/focus);
roc = focus/cos(phi);

dphi = 2*phi/numR;

z = roc.*cos(0:dphi:phi)-focus;
z = [fliplr(z(2:end)) z]';
r = roc.*sin(0:dphi:phi);
r = [-1*fliplr(r(2:end)) r]';
r = (iR+dr/2)+r;

theta = 0:dTheta:2*pi-dTheta;
numTheta = length(theta);
r = linspace(iR,oR,numR+1)';
theta = repmat(theta,size(r,1),1);
r = repmat(r,1,size(theta,2));
z = repmat(z,1,size(theta,2));
theta = reshape(theta,[],1);
r = reshape(r,[],1);
x = r.*cos(theta);
y = r.*sin(theta);
z = reshape(z,[],1);

coord = [x y z];

mathElePos = zeros(numR*numTheta*2,3*3);
for thetaIndex = 1:numTheta-1
    for rIndex = 1:numR
        mathElePos(((numR*(thetaIndex-1)+rIndex)*2-1),:) = [ ...
            coord((numR+1)*(thetaIndex-1)+rIndex,:) ...
            coord((numR+1)*(thetaIndex-1)+rIndex+1,:) ...
            coord((numR+1)*(thetaIndex)+rIndex,:)];
        mathElePos((numR*(thetaIndex-1)+rIndex)*2,:) = [ ...
            coord((numR+1)*(thetaIndex-1)+rIndex+1,:) ...
            coord((numR+1)*(thetaIndex)+rIndex,:) ...
            coord((numR+1)*(thetaIndex)+rIndex+1,:)];
    end
end
% edge case at the end
for rIndex = 1:numR
    
    mathElePos(((numR*(numTheta-1)+rIndex)*2-1),:) = [ ...
            coord((numR+1)*(numTheta-1)+rIndex,:) ...
            coord((numR+1)*(numTheta-1)+rIndex+1,:) ...
            coord(rIndex,:)];
    mathElePos((numR*(numTheta-1)+rIndex)*2,:) = [ ...
            coord((numR+1)*(numTheta-1)+rIndex+1,:) ...
            coord(rIndex,:) ...
            coord(rIndex+1,:)];
end

data = [ones(size(mathElePos,1),1) mathElePos ones(size(mathElePos,1),1)];
center = [0 0 0];
focus = [0 0 1];

Th = xdc_triangles (data, center, focus);

[impulseResponse]=defineImpResp(fractionalBandwidth,centerFrequency,FIELD_PARAMS);
end