See the git commit history for an effective change log pre-v6.3.1

v6.4.0
======
* Add more post-processing functionality (``post/res_sim.py``)

